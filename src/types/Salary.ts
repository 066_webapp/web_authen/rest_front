import type SalaryDetail from "./SalaryDetail";

export default interface Salary {
  id?: number;
  date_start: Date;
  date_end: Date;
  date_salary: Date;
  total?: number;
  salaryDetailLists: SalaryDetail[];
  updatedDate?: Date;
  deletedDate?: Date;
}
