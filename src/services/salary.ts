import type SalaryMgmt from "@/types/Salary";
import http from "./axios";

function getSalaryMgmts() {
  return http.get("/salaries");
}
function getSalaryYearMgmts() {
  return http.get("/salaries/year");
}
function getOneSalaryMgmt(id: number) {
  return http.get(`/salaries/${id}`);
}

function saveSalaryMgmt(tablemgmt: SalaryMgmt) {
  return http.post("/salaries", tablemgmt);
}

function updateSalaryMgmt(id: number, tablemgmt: SalaryMgmt) {
  return http.patch(`/salaries/${id}`, tablemgmt);
}

function deleteSalaryMgmt(id: number) {
  return http.delete(`/salaries/${id}`);
}

export default {
  getSalaryMgmts,
  saveSalaryMgmt,
  updateSalaryMgmt,
  deleteSalaryMgmt,
  getOneSalaryMgmt,
  getSalaryYearMgmts,
};
