import materialService from "@/services/material";
import { ref, watch, computed } from "vue";
import { defineStore } from "pinia";
import type BillMaterial from "@/types/BillMaterial";
import billMatService from "@/services/bill-material";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type BillMaterialDetail from "@/types/BillMaterialDetail";

export const useBillMaterialStore = defineStore("bill-material", () => {
  const loadingStore = useLoadingStore();
  const billMaterials = ref<BillMaterial[]>([]);
  const billMatDetails = ref<BillMaterialDetail[]>([]);
  const billMaterialsAt = ref<BillMaterial>();
  const messageStore = useMessageStore();
  const editedBillMaterial = ref<BillMaterial>({
    shop_name: "",
    buy: 0,
    total: 0,
    change: 0,
    employeeId: 5,
  });
  const dialog = ref(false);
  const editedBillItem = ref<BillMaterialDetail>({
    materialId: 1,
    quantity: 1,
    price: 0,
    total: 0,
  });
  const dialogD = ref(false);

  async function getBillMaterials() {
    loadingStore.isLoading = true;
    try {
      const res = await billMatService.getBillMaterials();
      billMaterials.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถดึง Bill Material ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getBillItemsAt(id: number) {
    loadingStore.isLoading = true;
    try {
      const bill = await billMatService.getOneBillMaterials(id);
      billMaterialsAt.value = bill.data;
      console.log(billMaterialsAt.value?.billItems);
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถดึง Bill Material detail ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveBillMaterial() {
    loadingStore.isLoading = true;
    try {
      console.log(editedBillMaterial.value.id);
      if (editedBillMaterial.value.id != undefined) {
        editedBillMaterial.value.billItems = undefined;
        await billMatService.updateBillMaterials(
          editedBillMaterial.value.id,
          editedBillMaterial.value
        );
        console.log(editedBillMaterial.value);
      } else {
        console.log(editedBillMaterial.value);
        await billMatService.saveBillMaterials(editedBillMaterial.value);
      }
      dialog.value = false;
      await getBillMaterials();
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถบันทึก BillMaterial Management ได้");
    }
    loadingStore.isLoading = false;
  }

  function clear() {
    editedBillMaterial.value = {
      shop_name: "",
      buy: 0,
      total: 0,
      change: 0,
    };
  }

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      clear();
    }
  });

  function editBillMaterial(billMaterial: BillMaterial) {
    loadingStore.isLoading = true;
    editedBillMaterial.value = JSON.parse(JSON.stringify(billMaterial));
    editedBillMaterial.value.employeeId = billMaterial.employee!.id;
    console.log(editedBillMaterial);
    dialog.value = true;
    loadingStore.isLoading = false;
  }

  const saveBillItems = async (billId: number) => {
    await getBillItemsAt(billId);

    const id = billMaterialsAt.value?.id;
    console.log(billMaterialsAt.value);
    const updateBillMaterial = ref<BillMaterial>({
      shop_name: billMaterialsAt.value!.shop_name,
      buy: billMaterialsAt.value!.buy,
      total: billMaterialsAt.value!.total,
      change: billMaterialsAt.value!.change,
      employeeId: billMaterialsAt.value!.employeeId,
      billItems: [], //billMaterialsAt.value!.billItems,
    });
    console.log(updateBillMaterial.value.billItems);
    billMatDetails.value.forEach((element) => {
      updateBillMaterial.value.billItems!.push({
        materialId: element.materialId,
        quantity: element.quantity,
        price: element.price!,
        // total: element.total,
        total: element.quantity * element.price!,
      });
    });
    console.log(updateBillMaterial.value.billItems);

    try {
      await billMatService.updateBillMaterials(id!, updateBillMaterial.value!);
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถบันทึก bill detail ได้");
    }

    await getBillItemsAt(billId);
  };

  function editBillItem(billItem: BillMaterialDetail) {
    console.log(billItem.material);
    loadingStore.isLoading = true;
    editedBillItem.value = JSON.parse(JSON.stringify(billItem));
    editedBillItem.value.materialId = billItem.material!.id;
    dialogD.value = true;
    loadingStore.isLoading = false;
  }

  const getMatPrice = async () => {
    const res = await materialService.getOneMaterial(
      editedBillItem.value.materialId!
    );
    const material = res.data;
    editedBillItem.value.price = material.unit_price;
    console.log(material);

    editedBillItem.value.total = computed(
      () => editedBillItem.value.quantity * editedBillItem.value.price
    ).value;
  };

  function clearDetail() {
    editedBillItem.value = {
      quantity: 1,
      price: 0,
    };
  }

  watch(dialogD, (newDialog, oldDialog) => {
    if (!newDialog) {
      clearDetail();
    }
  });

  function deleteBillItem(id: number) {
    loadingStore.isLoading = true;
    editedBillItem.value.id = id;
    messageStore.showConfirm(
      "Do you want to delete this bill item?",
      "billitem"
    );
    loadingStore.isLoading = false;
  }

  async function confirmDeleteItem() {
    try {
      await billMatService.deleteBillItem(editedBillItem.value.id!);
      await getBillItemsAt(billMaterialsAt.value!.id!);
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถลบ Bill Item ได้");
    }
    loadingStore.isLoading = false;
  }

  function deleteBill(id: number) {
    loadingStore.isLoading = true;
    editedBillMaterial.value.id = id;
    messageStore.showConfirm("Do you want to delete this bill?", "bill");
    loadingStore.isLoading = false;
  }

  async function confirmDelete() {
    try {
      await billMatService.deleteBillMaterial(editedBillMaterial.value.id!);
      await getBillMaterials();
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถลบ Bill ได้");
    }
    loadingStore.isLoading = false;
  }

  return {
    billMaterials,
    getBillMaterials,
    billMaterialsAt,
    getBillItemsAt,
    saveBillMaterial,
    dialog,
    editedBillMaterial,
    clear,
    editBillMaterial,
    billMatDetails,
    saveBillItems,
    dialogD,
    editedBillItem,
    clearDetail,
    getMatPrice,
    editBillItem,
    deleteBillItem,
    confirmDeleteItem,
    deleteBill,
    confirmDelete,
  };
});
