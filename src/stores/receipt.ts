import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Receipt from "@/types/Receipt";
import type ReceiptCreate from "@/types/ReceiptCreate";
import receiptService from "@/services/receipt";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type TableMgmt from "@/types/TableManagement";
import type ReceiptDetail from "@/types/ReceiptDetail";
import employeeService from "@/services/employee";
import { useLoginStore } from "./login";

export const useReceiptStore = defineStore("receipt", () => {
  const loadingStore = useLoadingStore();
  const receipts = ref<Receipt[]>([]);
  const receiptsAt = ref<Receipt>();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const dialogP = ref(false);
  const showPay = ref(false);
  const idReceipt = ref(0);
  const tableCheckBill = ref(0);
  const showBill = ref(false);
  const showText = ref(false);
  const loginStore = useLoginStore();
  const uuidReceipt = ref("");
  const editedReceipt = ref<Receipt>({
    // subtotal: 0,
    discount: 1,
    // total: 0,
    // received: 1,
    received: 0,
    change: 0,
    status: "รอชำระเงิน",
    payment: "-",
    // table: <TableMgmt>{},
  });
  const createR = ref(true);
  const editedReceiptCreate = ref<ReceiptCreate>({
    // subtotal: 0,
    discount: 1,
    // total: 0,
    received: 1,
    // change: 0,
    status: "รอชำระเงิน",
    payment: "-",
    tableid: 0,
  });
  const listReceipts = ref<String[]>([]);
  const showBillPay = ref<Receipt>({
    received: 0,
    change: 0,
    status: "รอชำระเงิน",
    payment: "-",
  });

  async function getReceipts() {
    loadingStore.isLoading = true;
    try {
      const res = await receiptService.getReceipts();
      const list = ref<String[]>([]);
      receipts.value = res.data;
      // receipts.value.forEach((element) => {
      //   list.value.push(element.employee.id);
      // });
      console.log(res);
      listReceipts.value = list.value;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถดึง Receipts ได้");
    }
    loadingStore.isLoading = false;
  }
  const Employeeid = () => {
    const items = [];
    for (let i = 0; i < receipts.value.length; i++) {
      items.push(receipts.value[i].employee?.id);
    }
    return items;
  };
  async function getReceiptitemAt(id: number) {
    loadingStore.isLoading = true;
    try {
      const rec = await receiptService.getOneReceipts(id);
      receiptsAt.value = rec.data;
      console.log(receiptsAt.value?.receiptDetail);
      console.log(receiptsAt.value);
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถดึง Receipt item ได้");
    }
    loadingStore.isLoading = false;
  }
  async function getOneReceiptsByUUid(uuidI: string) {
    loadingStore.isLoading = true;
    try {
      const rec = await receiptService.getOneReceiptsByUuid(uuidI);
      receiptsAt.value = rec.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถดึง Receipt ได้");
    }
    loadingStore.isLoading = false;
  }
  function editReceipt(receipt: Receipt) {
    loadingStore.isLoading = true;
    editedReceipt.value = JSON.parse(JSON.stringify(receipt));
    dialog.value = true;
    loadingStore.isLoading = false;
  }
  function deleteReceipt(id: number, num: number) {
    loadingStore.isLoading = true;
    editedReceipt.value.id = id;
    editedReceipt.value.table_num = num;
    // console.log(tableMgmts.value[id].num);
    messageStore.showConfirm(
      "ต้องการลบออเดอร์โต๊ะที่ " + editedReceipt.value.table_num + " หรือไม่",
      "receipt"
    );
    loadingStore.isLoading = false;
  }
  async function confirmDelete() {
    try {
      await receiptService.deleteReceipts(editedReceipt.value.id!);
      await getReceipts();
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถลบ Receipt Management ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveReceipt() {
    loadingStore.isLoading = true;
    try {
      if (editedReceipt.value.id) {
        console.log("----");
        console.log(editedReceipt.value);
        await receiptService.updateReceipts(
          editedReceipt.value.id,
          editedReceipt.value
        );
      } else {
        console.log(editedReceiptCreate.value);
        await receiptService.saveReceipts(editedReceiptCreate.value);
        // idReceipt.value = editedReceipt.value.id;
      }
      dialog.value = false;
      // console.log(editedTableMgmt.value);
      await getReceipts();
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถบันทึก Receipt Management ได้");
    }
    loadingStore.isLoading = false;
  }

  async function openR(table: TableMgmt) {
    // console.log(receipts.value[2]);
    // console.log(table.id);
    // console.log(
    //   receipts.value[2].table.id == table.id &&
    //     receipts.value[2].status == "รอชำระเงิน"
    // );
    await getReceipts();
    thisRec(table);
    console.log(createR.value);
    if (createR.value) {
      idReceipt.value = 0;
      editedReceiptCreate.value.tableid = table.id!;
      console.log(editedReceiptCreate.value.tableid);
      console.log(editedReceiptCreate.value);
      await saveReceipt();
      thisRec(table);
    }
    console.log(idReceipt.value);
  }

  function thisRec(table: TableMgmt) {
    idReceipt.value = 0;
    for (const rec of receipts.value) {
      if (rec.table!.id == table.id && rec.status == "รอชำระเงิน") {
        // console.log(rec.id);
        idReceipt.value = rec.id!;
        uuidReceipt.value = rec.uuidI!;
        createR.value = false;
        break;
      }
    }
    if (idReceipt.value == 0) {
      createR.value = true;
    }
  }

  async function updateTableRec(oldTable: TableMgmt, newTable: TableMgmt) {
    thisRec(oldTable);
    console.log(idReceipt.value);
    const res = await receiptService.getOneReceipts(idReceipt.value);
    const goalReceipt = res.data;
    console.log(goalReceipt);
    editedReceiptCreate.value = {
      // id: goalReceipt.id,
      discount: goalReceipt.discount,
      received: goalReceipt.received,
      status: goalReceipt.status,
      payment: goalReceipt.payment,
      tableid: newTable.id!,
    };
    console.log(editedReceiptCreate.value);
    // await saveReceipt();
    await receiptService.updateReceipts(
      goalReceipt.id,
      editedReceiptCreate.value
    );
    await getReceipts();
  }
  async function cancelTableRec(table: TableMgmt) {
    thisRec(table);
    receiptService.deleteReceipts(idReceipt.value);
    await getReceipts();
  }

  async function getDetails() {
    await getReceipts();
    const id = ref(0);
    for (const rec of receipts.value) {
      if (rec.table!.id == tableCheckBill.value && rec.status == "รอชำระเงิน") {
        id.value = rec.id!;
        break;
      }
    }
    console.log(tableCheckBill.value);
    console.log(id.value);
    getReceiptitemAt(id.value);
  }

  async function updateBill(rec: number, itemRec: Receipt) {
    try {
      const resM = await receiptService.getOneReceipts(rec!);

      const resBill = resM.data;

      const resE = await employeeService.getOneEmployee(
        parseInt(loginStore.userIdNow)
      );
      const empDo = resE.data;

      if (resBill.status === "รอชำระเงิน") {
        resBill.received = itemRec.received;
        resBill.change = itemRec.change;
        resBill.status = itemRec.status;
        resBill.payment = itemRec.payment;
        resBill.receiptDetail = undefined;
        resBill.employee = empDo;

        await receiptService.updateReceipts(resBill.id, resBill);
      } else {
        messageStore.showMessage("ไม่สามารถบันทึก Bill ได้");
      }
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถบันทึก Bill ได้");
    }
  }

  return {
    receipts,
    getReceipts,
    receiptsAt,
    getReceiptitemAt,
    deleteReceipt,
    editReceipt,
    saveReceipt,
    dialog,
    editedReceipt,
    listReceipts,
    Employeeid,
    confirmDelete,
    idReceipt,
    openR,
    updateTableRec,
    cancelTableRec,
    showBill,
    getDetails,
    tableCheckBill,
    showPay,
    showBillPay,
    updateBill,
    showText,
    getOneReceiptsByUUid,
    uuidReceipt,
    dialogP,
  };
});
