import { ref } from "vue";
import { defineStore } from "pinia";
import type Salary from "@/types/Salary";
import salaryService from "@/services/salary";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type SalaryDetail from "@/types/SalaryDetail";

export const useSalaryStore = defineStore("salary", () => {
  const loadingStore = useLoadingStore();
  const salaries = ref<Salary[]>([]);
  const salaryDetails = ref<SalaryDetail[]>([]);
  const salaryDetailsAt = ref<SalaryDetail>();
  const salariesAt = ref<Salary>();
  const messageStore = useMessageStore();
  const day = ["01"];
  // const month = [
  //   "มกราคม",
  //   "กุมภาพันธ์",
  //   "มีนาคม",
  //   "เมษายน",
  //   "พฤษภาคม",
  //   "มิถุนายน",
  //   "กรกฎาคม",
  //   "สิงหาคม",
  //   "กันยายน",
  //   "ตุลาคม",
  //   "พฤศจิกายน",
  //   "ธันวาคม",
  // ];

  const month = [
    { valuem: "01", mnth: "มกราคม" },
    { valuem: "02", mnth: "กุมภาพันธ์" },
    { valuem: "03", mnth: "มีนาคม" },
    { valuem: "04", mnth: "เมษายน" },
    { valuem: "05", mnth: "พฤษภาคม" },
    { valuem: "06", mnth: "มิถุนายน" },
    { valuem: "07", mnth: "กรกฎาคม" },
    { valuem: "08", mnth: "สิงหาคม" },
    { valuem: "09", mnth: "กันยายน" },
    { valuem: "10", mnth: "ตุลาคม" },
    { valuem: "11", mnth: "พฤศจิกายน" },
    { valuem: "12", mnth: "ธันวาคม" },
  ];

  const monthNum = [
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12",
  ];
  const yearList = ref([]);
  const year: string[] = [];
  function days() {
    for (let i = 2; i <= 31; i++) {
      if (i < 10) {
        day.push("0" + i);
      } else {
        day.push("" + i);
      }
    }
    console.log(day);
  }
  // const seatTableMgmt = () => {
  //   const items = [];
  //   for (let i = 0; i < tableMgmts.value.length; i++) {
  //     items.push(tableMgmts.value[i].seat);
  //   }
  //   for (let i = 0; i < items.length; i++) {
  //     for (let j = i + 1; j < items.length; j++) {
  //       if (items[i] === items[j]) {
  //         items.splice(i, 1);
  //       }
  //     }
  //   }

  //   console.log(items);
  //   return items;
  // };
  async function getYear() {
    try {
      const years = await salaryService.getSalaryYearMgmts();
      yearList.value = years.data;
      // JSON.parse(year.value);
      // for (let i = 0; i < year.value.length; i++) {
      //   const t = JSON.parse(year.value[i]);
      //   console.log(t);
      // }
      for (let i = 0; i < yearList.value.length; i++) {
        const t = JSON.stringify(yearList.value[i]);
        year.push(t.toString().substring(9, 13));
        // console.log(t.toString().substring(9, 13));
      }

      // t = JSON.parse(t);
      // t.toString().substring(8, 12);
      // console.log(t);
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถดึง Year Salary ได้");
    }
  }
  async function getSalaryMgmts() {
    loadingStore.isLoading = true;
    try {
      const res = await salaryService.getSalaryMgmts();
      salaries.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถดึง Salary ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getSalaryMgmtsAt(id: number) {
    loadingStore.isLoading = true;
    try {
      const salary = await salaryService.getOneSalaryMgmt(id);
      salariesAt.value = salary.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("ไม่สามารถดึง Salary Detail ได้");
    }
    loadingStore.isLoading = false;
  }

  return {
    salaries,
    getSalaryMgmts,
    salariesAt,
    getSalaryMgmtsAt,
    month,
    days,
    day,
    getYear,
    year,
    monthNum,
  };
});
